template <typename T>
SingletonBase<T>::SingletonBase()
{
    SingletonStack::get_instance()->push(this);
}

template <typename T>
SingletonBase<T>::~SingletonBase()
{
    SingletonStack::get_instance()->addRemoved(this);
}

