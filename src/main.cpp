#include <client/Authenticator.h>
#include <client/ConfigurationManager.h>
#include <ui/UIManager.h>

int main()
{
    auto authenticator = gix::client::Authenticator::get_instance();
    while (!authenticator->authenticate()) {
        std::cerr << "Incorrect username or password." << std::endl;
    }
    auto configManager = gix::client::ConfigurationManager::get_instance();
    if (!configManager->readConfigs()) {
        std::cerr << "Damaged config file.";
        return 0;
    }
    gix::ui::UIManager::get_instance()->init_main_view();
    return 0;
}
