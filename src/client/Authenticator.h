#pragma once

#include <utility/Macros.h>

namespace gix {
namespace client {

class Authenticator SINGLETON_BASE(RecursiveSpin)
{
    SINGLETON_DECL(Authenticator)

public:
    bool authenticate();
};

} // namespace client
} // namespace gix
